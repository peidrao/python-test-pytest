import pytest
from fibonacci.dynamic import fibonacci_dynamic
from fibonacci.cached import fibonacci_cached, fibonacci_lru_cached
from fibonacci.naive import fibonacci_naive
from typing import Callable


#code duplication / hard time debuggin
def test_naive() ->None:
    res = fibonacci_naive(0)
    assert res == 0

    res = fibonacci_naive(1)
    assert res == 1

    res = fibonacci_naive(2)
    assert res == 1

    res = fibonacci_naive(3)
    assert res == 2

@pytest.mark.parametrize('fib_func', [fibonacci_naive, fibonacci_cached, fibonacci_lru_cached, fibonacci_dynamic],)
@pytest.mark.parametrize('n,expected', [(0,0), (1,1), (2,1), (3,2)])
def test_naive_refactor(time_tracker, fib_func: Callable[[int], int], n: int, expected: int) -> None:
    res = fib_func(n)
    assert res == expected
