
from rest_framework import status, viewsets, pagination
from .models import Company 
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.core.mail import send_mail
from .serializers import CompanySerializer


class CompanyViewSet(viewsets.ModelViewSet):
    serializer_class = CompanySerializer
    queryset = Company.objects.all()
    pagination_class = pagination.PageNumberPagination



@api_view(http_method_names=['POST'])
def send_company_email(request):
    send_mail(
            subject='my cool subject', 
            message='my cool message', 
            from_email='peidrao01@gmail.com',
            recipient_list=['peidrao01@gmail.com'])
    return Response({'info': 'email sent successfully'}, status=status.HTTP_200_OK)
